/*
 * uart.c
 *
 *  Created on: 22/11/2012
 *      Author: Zhen
 */
#include <string.h>
#include "misc.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_usart.h"
#include "uart.h"
#include "raspi.h"
#include "led.h"

#define HASH_0_IRQ_PIN		GPIO_Pin_0
#define HASH_1_IRQ_PIN		GPIO_Pin_1
#define HASH_2_IRQ_PIN		GPIO_Pin_2
#define HASH_3_IRQ_PIN		GPIO_Pin_3
#define HASH_4_IRQ_PIN		GPIO_Pin_4
#define HASH_5_IRQ_PIN		GPIO_Pin_5
#define HASH_6_IRQ_PIN		GPIO_Pin_6
#define HASH_7_IRQ_PIN		GPIO_Pin_7
#define HASH_IRQ_PORT			GPIOC

#define HASH_0_CS_PIN			GPIO_Pin_0
#define HASH_1_CS_PIN			GPIO_Pin_1
#define HASH_2_CS_PIN			GPIO_Pin_6
#define HASH_3_CS_PIN			GPIO_Pin_7
#define HASH_4_CS_PIN			GPIO_Pin_8
#define HASH_5_CS_PIN			GPIO_Pin_9
#define HASH_6_CS_PIN			GPIO_Pin_12
#define HASH_7_CS_PIN			GPIO_Pin_13
#define HASH_CS_PORT			GPIOB


volatile uint8_t UART1_Rx_Buffer[UART_RX_BUF_BYTES];
volatile uint16_t UART1_Rx_Buffer_RdPtr;
volatile uint16_t UART1_Rx_Buffer_WrPtr;

volatile uint8_t UART1_Sending = 0;

__inline void UART_Delay(uint32_t ms)
{
	volatile uint16_t i,j;
	for (i=0;i<ms;i++){
		for (j=0;j<4000;j++){
			;
		}
	}	
}

void UART_Init(uint32_t BaudRate)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	UART1_Sending = 0;

	//CS
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(HASH_CS_PORT, &GPIO_InitStructure);

	/*UART 1 RX*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*UART 1 TX*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* USART1 TX DMA1 Channel (triggered by USART Tx event) Config */
	DMA_DeInit(DMA1_Channel4);
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(USART1->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = 0;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 2;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);

	/* Configure USART1 */
	USART_InitStructure.USART_BaudRate = BaudRate;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);

	DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART1, ENABLE);

  /* Enable USART DMA TX request */
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
}

/*
void UART1_Write(void *Data, uint8_t Block)
{
	while (UART1_Sending);
	UART1_Sending = 1;
	//Disable DMA1 Channel4
	DMA1_Channel4->CCR &= ((uint32_t)0xFFFFFFFE);
	//Write to DMA1 Channel4 CPAR 
	DMA1_Channel4->CMAR = (uint32_t)Data;
	//Reset DMA1 Channel4 remaining bytes register 
	DMA1_Channel4->CNDTR = DB_DATA_SIZE;
	//Enable DMA1 Channel4
	DMA1_Channel4->CCR |= ((uint32_t)0x00000001);
	if (Block)
	{
		while(UART1_Sending);
	}
}
*/

void UART1_Write(uint8_t *Data, uint8_t Block)
{
	uint16_t i;
	for (i=0;i<DB_DATA_SIZE;i++) {
    USART_SendData(USART1, *Data);
		Data ++;
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
    {
    }
  }
}
__inline void UART_CS(uint8_t cs, uint8_t level)
{	uint16_t mask;
	if (level ==0) {
		GPIO_SetBits(HASH_CS_PORT,0x0f00);
		mask = ((~cs)&0xff) << 8;
		GPIO_ResetBits(HASH_CS_PORT,mask);
	} else {
		GPIO_SetBits(HASH_CS_PORT,0x0f00);
	}
}

void UART_Work(void)
{
	uint8_t i;
	uint8_t board;
	uint8_t pingpong = RASPI_PingPong == 0 ? 1:0;
	uint16_t ptr = 0;
	uint8_t cnt;
	for (board=0;board<8;board++) {
  	if (RASPI_Buff_Switch == 1) {
    	if ((RASPI_Rx_Buff[pingpong][0][1]!=0x00)&&(RASPI_Rx_Buff[pingpong][0][1]!=0x01)&&(RASPI_Rx_Buff[pingpong][0][1]!=0x88)) {
	  	  UART_Delay(100);
	      NVIC_SystemReset();
		  }
		  RASPI_Buff_Switch = 0;
	    RASPI_Reset_DMA();
		  return;
    }
		ptr = 0;
  	UART1_Rx_Buffer_RdPtr = 0;
	  UART1_Rx_Buffer_WrPtr = 0;
		UART_CS(board,0);
		UART_Delay(1);
		cnt = 0;
		for (i=0;i<RASPI_BUFF_SIZE;i++) {
		  if ((RASPI_Rx_Buff[pingpong][i][0] == DB_START_SENTINEL)&&(RASPI_Rx_Buff[pingpong][i][DB_DATA_SIZE-1] == board))	{
			  RASPI_Rx_Buff[pingpong][i][DB_DATA_SIZE-1] = DB_FINISH_SENTINEL;
			  UART1_Write((uint8_t *)&RASPI_Rx_Buff[pingpong][i][0],1);	
				RASPI_Rx_Buff[pingpong][i][0] = 0x88;
  			if (cnt>10) {
	  			break;
		  	}
			  cnt ++;
		  }
	  }
		if (cnt < 5) {
			UART_Delay(1);
		}
		UART_Delay(1);
		UART_CS(board,1);
		//Wait 1 miliseconds so hash board can finish started transmission 	
		UART_Delay(1);
	  if (UART1_Rx_Buffer_WrPtr != 0) {
		  while(ptr+DB_DATA_SIZE-2<UART1_Rx_Buffer_WrPtr){
			  while(UART1_Rx_Buffer[ptr] != DB_START_SENTINEL){
				  ptr++;
		  	}
			  if((ptr+DB_DATA_SIZE-2<UART1_Rx_Buffer_WrPtr)&&(UART1_Rx_Buffer[ptr+DB_DATA_SIZE-1] == DB_FINISH_SENTINEL)) {
					UART1_Rx_Buffer[ptr+DB_DATA_SIZE-1] = board;
				  memcpy((void *)RASPI_Tx_Buff[pingpong][RASPI_Tx_Ptr], (void *)&UART1_Rx_Buffer[ptr],DB_DATA_SIZE);
					if (RASPI_Tx_Ptr < RASPI_BUFF_SIZE - 1) {
						RASPI_Tx_Ptr++;
					}
				  ptr += DB_DATA_SIZE;
			  } else {
				  ptr++;
			  }
		  }
	  }
	}	
}
