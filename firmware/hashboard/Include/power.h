/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POWER_H
#define __POWER_H

#ifdef __cplusplus
 extern "C" {
#endif

#define POWER_PORT		GPIOB
#define POWER_PIN_VIDS 	GPIO_Pin_6 | GPIO_Pin_7	| GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10
#define POWER_PIN_EN 	GPIO_Pin_12

#define POWER_ON 		GPIO_WriteBit(POWER_PORT, POWER_PIN_EN , Bit_SET); 
#define POWER_OFF 		GPIO_WriteBit(POWER_PORT, POWER_PIN_EN , Bit_RESET); 

void POWER_Init(void);
void POWER_SetVolt(uint8_t volt);

#ifdef __cplusplus
}
#endif
   
#endif
