/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LED_H
#define __LED_H

#ifdef __cplusplus
 extern "C" {
#endif

#define LED_UART_PIN 	GPIO_Pin_1
#define LED_CHIP_PIN 	GPIO_Pin_5

#define LED_UART_ON		GPIOB->BSRR = GPIO_Pin_1;
#define LED_UART_OFF	GPIOB->BRR = GPIO_Pin_1;
#define LED_CHIP_ON		GPIOB->BSRR = GPIO_Pin_5; 
#define LED_CHIP_OFF	GPIOB->BRR = GPIO_Pin_5;

void LED_Init(void);

#ifdef __cplusplus
}
#endif
   
#endif
