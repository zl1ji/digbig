/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WORKPOOL_H
#define __WORKPOOL_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "parcel.h"
#include "protocol.h"
#include "fb1600.h"
	 
#define NONCE_CORE_POOL_SIZE  	8  
   
typedef __packed struct {
     uint32_t UniqueId;
     uint8_t CoreWorkId;
     uint8_t MidState[32];
     uint8_t RestData[12];
   } NonceCoreWork;
   
typedef __packed struct {
     NonceCoreWork Works[NONCE_CORE_POOL_SIZE];
     uint8_t Write_Ptr;
     uint8_t Read_Ptr;
	   uint8_t Result_Ptr;
     uint8_t Available;
	   uint8_t DataDepth;
	   uint8_t CoreDataDepth;
   } NonceCore;

extern volatile uint32_t lastWorkTick;

void Pool_Init(uint32_t difficulty);
void Pool_AddWork(StructHashData *dataDown);
void Pool_Task(void);

#ifdef __cplusplus
}
#endif

#endif
