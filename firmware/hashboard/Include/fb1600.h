/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FB1600_H
#define __FB1600_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"

#define NUMBER_OF_CORES         32   
	 
extern volatile uint8_t ChipStats[8];
extern volatile uint8_t CoreOK[32];

void FB1600_Init(void);
void FB1600_Reset(uint8_t pll,uint32_t difficulty);
uint8_t FB1600_Ready(uint8_t core);
void FB1600_Comm(uint8_t core, uint8_t *sendbuffer);

#ifdef __cplusplus
}
#endif

#endif
