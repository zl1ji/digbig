#include <string.h>
#include "stm32f10x.h"
#include "stm32f10x_it.h"
#include "uart.h"
#include "parcel.h"
#include "workpool.h"
#include "fb1600.h"
#include "power.h"
#include "led.h"
#include "temp.h"

void Delay(int16_t ms)
{
	volatile uint16_t i,j;
	for (i=0;i<ms;i++){
		for (j=0;j<11000;j++){
			;
		}
	}
}

int main()
{	
#ifdef PRODUCTION
	  int i,j,k;
#endif	
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_USART1 | RCC_APB2Periph_SPI1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO, ENABLE);
    
	// Disable JTAG
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);

    SysTick_Config(72000); //1ms
	  TEMP_Init();
    LED_Init();
#ifdef PRODUCTION
    POWER_Init();
	  POWER_SetVolt(105);
    FB1600_Init();
	  if((ChipStats[0] == 0x0f)&&(ChipStats[1] == 0x0f)&&(ChipStats[2] == 0x0f)&&(ChipStats[3] == 0x0f)) {
			 LED_UART_ON;
		}
	  if((ChipStats[4] == 0x0f)&&(ChipStats[5] == 0x0f)&&(ChipStats[6] == 0x0f)&&(ChipStats[7] == 0x0f)) {
			 LED_CHIP_ON;
		}
	  POWER_OFF;
    UART_Init(115200);
		USART_SendData(USART1, 27);
		Delay(1);
		USART_SendData(USART1, '[');
		Delay(1);
		USART_SendData(USART1, '2');
		Delay(1);
		USART_SendData(USART1, 'J');
		Delay(1);
		USART_SendData(USART1, 10);
		Delay(1);
		USART_SendData(USART1, 13);
		Delay(1);
		for (i=0;i<8;i++)
		{
			for (j=0;j<4;j++) 
			{
				for (k=0;k<8;k++) 
				{
				  if (((0x01 << k) & CoreOK[(i*4)+j]) == (0x01 << k)) {
						USART_SendData(USART1, '.');
					} else {
						USART_SendData(USART1, 'X');
					}
					Delay(1);
				}
		  	USART_SendData(USART1, ' ');
			  Delay(1);
			}
			USART_SendData(USART1, 10);
			Delay(1);
			USART_SendData(USART1, 13);
			Delay(1);
		}
	  while(1) 
		{
		}
#else	//NOT PRODUCTION	
    POWER_Init();
	  POWER_SetVolt(90);
    FB1600_Init();
    Parcel_Init();
    Pool_Init(1);
#ifdef DIGBIGSPI
    UART_Init(1843200);
#else	//DIGBIGSPI
    UART_Init(115200);
#endif //DIGBIGSPI
	  POWER_OFF;
    while(1) {
	    Parcel_Work();
      Pool_Task();
			//Restart if no work received in 20 seconds
			if (SysTickCounter > lastWorkTick+20000) {
          NVIC_SystemReset();
			}
    }
#endif //PRODUCTION	
}


#ifdef USE_FULL_ASSERT
/*******************************************************************************
 * Function Name  : assert_failed
 * Description    : Reports the name of the source file and the source line number
 *                  where the assert_param error has occurred.
 * Input          : - file: pointer to the source file name
 *                  - line: assert_param error line source number
 * Output         : None
 * Return         : None
 *******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* Infinite loop */
    while (1)
    {}
}
#endif

