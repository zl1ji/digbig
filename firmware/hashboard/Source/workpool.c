#include <string.h>
#include "workpool.h"
#include "parcel.h"
#include "fb1600.h"
#include "led.h"

#define START_CORE	0
//#define STOP_CORE   3
#define STOP_CORE NUMBER_OF_CORES - 1

static volatile NonceCore WorkPool[NUMBER_OF_CORES];

extern volatile uint32_t SysTickCounter;
extern volatile uint8_t RxBuffer[45];

static volatile uint32_t LastEmptyTick = 0;
static volatile uint32_t Difficulty = 16;

volatile uint32_t lastWorkTick;

void Pool_Init(uint32_t difficulty)
{
  uint8_t i,j;
  Difficulty = difficulty;
  for (i=0;i<NUMBER_OF_CORES;i++) {
    if ((ChipStats[i >> 2] & (1 << (i % 4)))!=0) {
      WorkPool[i].Available = 1;
      WorkPool[i].Read_Ptr = 0;
      WorkPool[i].Write_Ptr = 0;
      WorkPool[i].Result_Ptr = 0;
	    WorkPool[i].DataDepth = 0;
	    WorkPool[i].CoreDataDepth = 0;
      for (j=0;j<NONCE_CORE_POOL_SIZE;j++) {
        WorkPool[i].Works[j].CoreWorkId = 0xff;
      }
    } else {
      WorkPool[i].Available = 0;
    }
  }
}

__inline NonceCoreWork *Pool_GetResPtrById(NonceCore *pool, uint8_t id)
{
	uint8_t resultptr = pool->Result_Ptr;
	if (resultptr == id) {
		pool->Result_Ptr = (resultptr+1) % NONCE_CORE_POOL_SIZE;
		pool->CoreDataDepth --; 
		return (NonceCoreWork *)&(pool->Works[resultptr]);
	} else {
		return 0;
	}
}

void Pool_Task()
{
  uint8_t i;
  uint8_t rxBase;
  uint8_t id;
  NonceCore *pool;
  NonceCoreWork *work;
  uint8_t readPtr;

  for (i=START_CORE;i<=STOP_CORE;i++) {
    pool = (NonceCore *)&WorkPool[i];
    if (pool->Available) {
      if ((pool->DataDepth != 0) && (FB1600_Ready(i))) {
        readPtr = pool->Read_Ptr;
        FB1600_Comm(i, (uint8_t *)&(pool->Works[readPtr].CoreWorkId));
        pool->Read_Ptr = (readPtr + 1) % NONCE_CORE_POOL_SIZE;
  		  pool->DataDepth--;
		    pool->CoreDataDepth ++;
        rxBase = (i%4)*5+6;
        id = RxBuffer[rxBase] & 0x0f;
        work = Pool_GetResPtrById(pool,id);
        if (work != 0) {
          if ((RxBuffer[rxBase]&0xf0)!=0) {
            uint32_t nonce = ((uint32_t)RxBuffer[rxBase+1] << 24); 
            nonce += ((uint32_t)RxBuffer[rxBase+2] << 16);
            nonce += ((uint32_t)RxBuffer[rxBase+3] << 8);
            nonce += (uint32_t)RxBuffer[rxBase+4];
            Parcel_SendData(work->UniqueId,nonce,Difficulty,i);
            continue;
          } else {
            Parcel_SendData(work->UniqueId,0,0,i);
            continue;
          }
        }
      } 
      if (pool->DataDepth + pool->CoreDataDepth < NONCE_CORE_POOL_SIZE-3) {
        if (SysTickCounter > LastEmptyTick) {
					if (parcelDataUp.NeedConfig != 0x00) {
            LastEmptyTick = SysTickCounter+500;
					} else {
            LastEmptyTick = SysTickCounter+100;
					}
          Parcel_SendData(0,0,0,i);
        }
			}
    }
  }  
}

void Pool_AddWork(StructHashData *dataDown)
{
  NonceCore *pool;
  NonceCoreWork *work;
  if (dataDown->Core >= NUMBER_OF_CORES)
  {
  	return;
  }
  pool = (NonceCore *)&(WorkPool[dataDown->Core]);
  if (pool->DataDepth + pool->CoreDataDepth < NONCE_CORE_POOL_SIZE) {
    uint8_t writePtr = pool->Write_Ptr;
	  work = &(pool->Works[writePtr]);
    work->CoreWorkId = writePtr | 0x10;
		work->UniqueId = dataDown->UniqueId;
    memcpy((void *)(&(work->MidState)), (void *)(&(dataDown->MidState)),44); //UniqueId and BlockHeader data
	
    writePtr ++;
    pool->Write_Ptr = writePtr % NONCE_CORE_POOL_SIZE;
	  pool->DataDepth++;
  }
	lastWorkTick = SysTickCounter;
}

