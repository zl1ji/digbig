#include "stm32f10x.h"
#include "power.h"
#include "led.h"
#include "parcel.h"

#ifdef VOLT_PLUS_025
const uint16_t VCODE[31] = {
	0x0880, //0.83
	0x0880, //0.84
  0x08c0, //0.850V
  0x08c0, //0.86
  0x0900, //0.875V
  0x0900, //0.88
  0x0900, //0.89
  0x0940, //0.900V
  0x0940, //0.91
  0x0980, //0.925V
  0x0980, //0.93
  0x0980, //0.94
  0x09c0, //0.950V
  0x09c0, //0.96
  0x0a00, //0.975V
  0x0a00, //0.98
  0x0a00, //0.99
  0x0a40, //1.000V
  0x0a40, //1.01
  0x0a80, //1.025V
  0x0a80, //1.03
  0x0a80, //1.04
  0x0ac0, //1.050V
  0x0ac0, //1.06
  0x0b00, //1.075V
  0x0b00, //1.08
  0x0b00, //1.09
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
};
#endif
#ifdef VOLT_PLUS_050
const uint16_t VCODE[31] = {
  0x08c0, //0.850V
  0x08c0, //0.86
  0x0900, //0.875V
  0x0900, //0.88
  0x0900, //0.89
  0x0940, //0.900V
  0x0940, //0.91
  0x0980, //0.925V
  0x0980, //0.93
  0x0980, //0.94
  0x09c0, //0.950V
  0x09c0, //0.96
  0x0a00, //0.975V
  0x0a00, //0.98
  0x0a00, //0.99
  0x0a40, //1.000V
  0x0a40, //1.01
  0x0a80, //1.025V
  0x0a80, //1.03
  0x0a80, //1.04
  0x0ac0, //1.050V
  0x0ac0, //1.06
  0x0b00, //1.075V
  0x0b00, //1.08
  0x0b00, //1.09
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
  0x0b40, //1.100V
};
#endif
#ifdef VOLT_PLUS_000
const uint16_t VCODE[31] = {
  0x0840, //0.800V
  0x0840, //0.81
	0x0880, //0.825V
	0x0880, //0.83
	0x0880, //0.84
  0x08c0, //0.850V
  0x08c0, //0.86
  0x0900, //0.875V
  0x0900, //0.88
  0x0900, //0.89
  0x0940, //0.900V
  0x0940, //0.91
  0x0980, //0.925V
  0x0980, //0.93
  0x0980, //0.94
  0x09c0, //0.950V
  0x09c0, //0.96
  0x0a00, //0.975V
  0x0a00, //0.98
  0x0a00, //0.99
  0x0a40, //1.000V
  0x0a40, //1.01
  0x0a80, //1.025V
  0x0a80, //1.03
  0x0a80, //1.04
  0x0ac0, //1.050V
  0x0ac0, //1.06
  0x0b00, //1.075V
  0x0b00, //1.08
  0x0b00, //1.09
  0x0b40, //1.100V
};
#endif

static volatile uint8_t volt_set;

void Delay(int16_t ms);

void POWER_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  /* Configure VID as Output */
  GPIO_InitStructure.GPIO_Pin = POWER_PIN_VIDS;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
  GPIO_Init(POWER_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  POWER_PIN_EN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(POWER_PORT, &GPIO_InitStructure);
}

void POWER_SetVolt(uint8_t volt)
{
  POWER_OFF;
  Delay(10);
	volt_set = volt;
  GPIO_SetBits(POWER_PORT, POWER_PIN_VIDS); // Set to lowest voltage
	if ((volt > 110)||(volt < 80)) {
		volt = 100;
	}
	GPIO_ResetBits(POWER_PORT, VCODE[volt-80]);
  Delay(2);
	POWER_ON;
  Delay(200);
	parcelDataUp.Volt = volt;
}
